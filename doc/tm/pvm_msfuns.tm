\function{pvm_ms_kill}
\synopsis{Send a "task complete" message to a specific task}
\usage{pvm_ms_kill (Int_Type mtid, Int_Type stid)}
\description
  This function may be used to send a "task complete" message
  to a specific PVM process.  The first argument gives
  the task identifier of the destination process.  The second
  argument gives the task identifier of the sending process.
\example
#v+
  tid = pvm_mytid ();
  ptid = pvm_parent ();
  pvm_ms_kill (ptid, tid);
#v-
\seealso{pvm_ms_slave_exit}
\done

\function{pvm_ms_set_num_processes_per_host}
\synopsis{Set the maximum number of simultaneous processes per host}
\usage{pvm_ms_set_num_processes_per_host (Int_Type num_processes)}
\description
  This function is used to set the maximum number of simultaneous
  processes per host.  The master process normally runs as many
  simultaneous processes as possible;  by setting the maximum
  number of simultaneous processes per host, one can limit the
  processing load per host.
\example
#v+
  pvm_ms_set_num_processes_per_host (2);
#v-
\seealso{pvm_ms_run_master}
\done

\function{pvm_ms_set_debug}
\synopsis{Set the debug flag}
\usage{pvm_ms_set_debug (Int_Type debug)}
\description
  This function may be used to control whether debugging
  information is printed out during execution.  Debugging
  information is printed if the flag is non-zero.
\example
#v+
  pvm_ms_set_debug (1);
#v-
\seealso{pvm_ms_set_num_processes_per_host}
\done

\function{pvm_ms_slave_exit}
\synopsis{Cause a normal exit of a slave process from the PVM}
\usage{pvm_ms_slave_exit (Int_Type exit_status)}
\description
  To exit the PVM, a slave process calls this function to
  send its exit status to the parent process and to notify
  the local \file{pvmd} of its exit.
\example
#v+
    pvm_ms_slave_exit (exit_status);
#v-
\seealso{pvm_ms_run_slave}
\done

\function{pvm_ms_run_slave}
\synopsis{Execute the slave's assigned task in a subshell, then exit the PVM}
\usage{pvm_ms_run_slave (String_Type argv[])}
\description
   A slave process calls this function to run a command in a
   subshell and then exit the PVM.  The command line is
   constructed by concatenting the elements of an array of
   strings, \var{argv}, delimited by spaces. The integer return
   value from the \sfun{system} call provides the exit status
   for the slave process.  After sending this value to its
   parent process, the slave notifies the PVM and exits.
\example
#v+
   pvm_ms_run_slave (argv);
#v-
\seealso{pvm_ms_slave_exit}
\done

\function{pvm_ms_run_master}
\synopsis{Submit a list of tasks to the PVM}
\usage{Struct_Type exit_status[] = pvm_ms_run_master (String_Type pgms[])}
\description
  This function is used to submit a managed list of tasks to
  the PVM.  The task list manager will try to ensure that all
  tasks are completed and, upon completion of the task list,
  will return an array of structures containing information
  about the results of each task.

\example
  To run the Unix command \it{ps xu} on a number of different
  hosts:
#v+
    variable slave_argv = Array_Type[n];
    slave_argv[*] = ["ps", "axu"];
    exit_status = pvm_ms_run_master (slave_argv);
#v-
\seealso{pvm_ms_add_new_slave}
\done

\function{pvm_ms_add_new_slave}
\synopsis{Add a new slave to the managed list}
\usage{pvm_ms_add_new_slave (String_Type argv[])}
\description
 This function may be used to add a new slave process
 while pvm_ms_run_master() is running, usually as a result
 of handling a message.
\example
#v+
    pvm_ms_add_new_slave ("vex");
#v-
\seealso{pvm_ms_run_master}
\done

\function{pvm_ms_set_message_callback}
\synopsis{Set a callback for handling user-defined messages}
\usage{pvm_ms_set_message_callback (Ref_Type func)}
\description
 This function may be used to handle user-defined messages
 be sent from slave processes back to the master process.
\example
#v+

 static define handle_user_message (msgid, tid)
 {
    switch (msgid)
      {
       case USER_SLAVE_RESULT:
           recv_results (tid);
           start_task (tid);
      }
      {
       case USER_SLAVE_READY:
           start_task (tid);
      }
      {
         % default:
           return 0;
      }
    return 1;
 }

 pvm_ms_set_message_callback (&handle_user_message);

#v-
\seealso{pvm_ms_set_idle_host_callback, pvm_ms_set_slave_exit_failed_callback}
\done

\function{pvm_ms_set_slave_exit_failed_callback}
\synopsis{Set a hook to be called when a slave exits on failure}
\usage{pvm_ms_set_slave_exit_failed_callback (Ref_Type func)}
\description

 This function may be used to have the master process perform a
 specified action whenever a slave process exits without having
 completed its assigned task.

 This is primarily useful in the context where each command-line
 submitted to \ifun{pvm_ms_run_master} represents a task which
 itself communicates with the PVM, performing potentially many
 additional tasks which are independently managed by the
 process that called \ifun{pvm_ms_run_master}.

 For example, consider a case in which initialization of slave
 processes is very expensive but, once initialized, a single
 slave process may perform many tasks.  In this case, the
 master process may spawn a small number of slaves and then
 repeatedly send each slave a task to perform.  Each slave
 performs its task, sends the result to the master, and then
 waits for another task. The managing process must keep track
 of which tasks have been completed and which remain. If a
 slave exits while working on a task, it is important that the
 manager process be notified that that task in progress was not
 completed and that it should be reassigned to another slave.

\example

#v+
   static define slave_exit_failed_callback (msgid, tid)
   {
      variable t = find_task_tid (tid);

      if (orelse {t == NULL} {t.status == FINISHED})
        return;

      % mark the unfinished task "READY" so that it will
      % be assigned to another slave

      t.tid = -1;
      t.status = READY;
   }

   pvm_ms_set_slave_exit_failed_callback (&slave_exit_failed_callback);
#v-

\seealso{pvm_ms_set_message_callback}
\done

\function{pvm_ms_set_slave_spawned_callback}
\synopsis{Set the slave spawned callback hook}
\usage{pvm_ms_set_slave_spawned_callback (Ref_Type func)}
\description

 This function may be used to specify a callback function to be
 called whenever a slave process has been spawned.  The callback
 function will be called with three arguments: the slave task
 id, the name of the host running the slave process, and an
 array of strings representing the argument list passed to the
 slave.

\example

#v+
   static define slave_spawned_callback (tid, host, argv)
   {
      vmessage ("Slave running %s spawned on %s with task-id %d",
                argv[0], host, tid);
   }
   pvm_ms_set_slave_spawned_callback (&slave_spawned_callback);
#v-

\seealso{pvm_ms_set_message_callback}
\done

\function{pvm_ms_set_idle_host_callback}
\synopsis{Set the idle host hook}
\usage{pvm_ms_set_idle_host_callback (Ref_Type func)}
\description
 This function may be used to specify a callback function to be
 called whenever a new host is added to the virtual machine.
\example

#v+
 static define idle_host_callback ()
 {
    loop (Max_Num_Processes_Per_Host)
      {
         variable slave_argv = build_slave_argv (0);
         pvm_ms_add_new_slave (slave_argv);
      }
 }
 pvm_ms_set_idle_host_callback (&idle_host_callback);
#v-

\seealso{pvm_ms_set_message_callback}
\done

\function{pvm_ms_set_hosts}
\synopsis{Set list of hosts to use}
\usage{pvm_ms_set_hosts (String_Type hosts[])}
\description
  This function may be used to specify which hosts will be used
  to perform distributed calculations.  The default is to use
  all hosts in the current PVM.
\example
#v+
      pvm_ms_set_hosts (["vex", "pirx", "aluche"]);
#v-
\seealso{pvm_addhosts}
\done

